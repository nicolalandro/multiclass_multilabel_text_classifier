import pickle

import keras
from keras.callbacks import ReduceLROnPlateau, EarlyStopping, ModelCheckpoint
from keras.layers import Dense, Embedding, GlobalMaxPool1D, Dropout
from keras.models import Sequential
from keras.optimizers import Adam
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MultiLabelBinarizer

TEXTS = ['testo 1', 'secondo testo', 'ultimo']
TAGS = [['classe 1'], ['classe 1', 'classe 2'], ['classe 2']]

tokenizer = Tokenizer(num_words=5000, lower=True)
tokenizer.fit_on_texts(TEXTS)
with open('tokenizer.pickle', 'wb') as handle:
    pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
sequences = tokenizer.texts_to_sequences(TEXTS)
maxlen = len(sorted(sequences, key=len)[-1])
x = pad_sequences(sequences, maxlen=maxlen)

multilabel_binarizer = MultiLabelBinarizer()
multilabel_binarizer.fit(TAGS)
with open('multilabel_binarizer.pickle', 'wb') as handle:
    pickle.dump(multilabel_binarizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
classes = multilabel_binarizer.classes_
y = multilabel_binarizer.transform(TAGS)
num_classes = len(classes)

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=9000)

model = Sequential()
model.add(Embedding(100, 20, input_length=maxlen))
model.add(Dropout(0.15))
model.add(GlobalMaxPool1D())
model.add(Dense(num_classes, activation='sigmoid'))

model.compile(optimizer=Adam(0.015), loss='binary_crossentropy', metrics=['categorical_accuracy'])
callbacks = [
    ReduceLROnPlateau(),
    EarlyStopping(patience=4),
    ModelCheckpoint(filepath='model-simple.h5', save_best_only=True)
]

history = model.fit(
    x_train, y_train,
    epochs=20,
    batch_size=32,
    validation_split=0.1,
    callbacks=callbacks
)

simple_model = keras.models.load_model('model-simple.h5')
metrics = simple_model.evaluate(x_test, y_test)
print("{}: {}".format(simple_model.metrics_names[0], metrics[0]))
print("{}: {}".format(simple_model.metrics_names[1], metrics[1]))

pred = simple_model.predict(x_test)
print(pred)
